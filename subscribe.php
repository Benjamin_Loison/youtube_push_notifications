#!/usr/bin/php

<?php

    require 'config.php';

    function subscribeYouTubeChannel($channelId, $subscribe = true)
    {
        global $callbackUrl;
        $subscribeUrl = 'https://pubsubhubbub.appspot.com/subscribe';
        $topicUrl = "https://www.youtube.com/xml/feeds/videos.xml?channel_id=$channelId";

        $data = [
            'hub.mode' => $subscribe ? 'subscribe' : 'unsubscribe',
            'hub.callback' => $callbackUrl,
            // Is it respected and necessary?
            'hub.lease_seconds' => 60 * 60 * 24 * /*365*/11,
            'hub.topic' => $topicUrl
        ];

        $opts = ['http' =>
            [
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($data)
            ]
        ];

        $context  = stream_context_create($opts);

        @file_get_contents($subscribeUrl, false, $context);

        return str_contains($http_response_header[0], '200');
    }

    $subscriptions = array_keys(json_decode(file_get_contents('subscriptions.json'), true));
    foreach($subscriptions as $channelId)
    {
        if(!subscribeYouTubeChannel($channelId) && false)
        {
            echo "Error for $channelId!";
            break;
        }
    }

?>
