<?php

    require 'config.php';

    function logg($toLog)
    {
        file_put_contents('log.txt', $toLog, FILE_APPEND);
    }

    // Test with: `curl -d '@input.xml' https://CENSORED/subscribe_callback.php`
    // TODO: consider sharing secret with the notifier to avoid anyone notifying me
    if (isset($_GET['hub_challenge'])) {
        echo $_REQUEST['hub_challenge'];
    } else {
        $video = parseYouTubeUpdate(file_get_contents('php://input'));
        logg(var_export($video, true));
    }

    function sendMatrixMessage($message)
    {
        $message = escapeshellarg($message);
        exec("matrix-commander -z -m $message");
    }

    // If receive deletion notification, could delete the message, assuming that it is still accessible but just not shown with the provided reason and if I have not read the notification yet.
    function parseYouTubeUpdate($data)
    {
        global $SAPISIDHASH, $__Secure_1PSID, $__Secure_1PAPISID, $__Secure_1PSIDTS;
        $xml = simplexml_load_string($data);
        $entry = $xml->entry;
        $yt = $entry->children('yt', true);

        $videoId = (string)$yt->videoId;
        $channelId = (string)$yt->channelId;
        $title = $entry->title;
        $channelName = $entry->author->name;

        // Remove on-website notification.
        // Could also disable receiving notifications for these channels but then we don't have YouTube UI specific icon to distinguish with other subscriptions.

        function getYouTubeOperationalAPI($url)
        {
            $url = "https://youtube.local/$url";
            // TODO: would be nice to self-sign and approve machine-wide the certificate. It doesn't seem doable due to the peer name verification, except if I explicitly request the localhost HTTPS certificate to contain the given domain name, no?
            $context = stream_context_create([
                'ssl' => [
                    'verify_peer_name' => false,
                ]
            ]);
            $dataStr = file_get_contents($url, false, $context);
            try
            {
                $data = json_decode($dataStr, true);
            }
            catch (Exception $e)
            {
                sendMatrixMessage('Not JSON!' . $dataStr);
                die();
            }
            return $data['items'][0];
        }

        $channelHandle = json_decode(file_get_contents('subscriptions.json'), true)[$channelId];
        $item = getYouTubeOperationalAPI("noKey/videos?part=snippet&id=$videoId");
        $snippet = $item['snippet'];

        if (
            ($channelHandle === '@28minutesARTE' && (!str_starts_with($title, 'Intéressant : ') && !str_contains($snippet['description'], '#Intéressant'))) ||
            ($channelHandle === '@Europe1' && !str_starts_with($title, 'Les origines ')) ||
            ($channelHandle === '@LeParisien' && preg_match("/^\[ *PODCAST\] /", $title) === 1) ||
            ($channelHandle === '@SympaCool' && !str_starts_with($title, 'Cocovoit '))
        )
        {
            return $data;
        }

        // Don't notify multiple times for the same video
        $alreadyNotifiedVideoIdsFilePath = 'already_notified_video_ids.json';
        $alreadyNotifiedVideoIds = json_decode(file_get_contents($alreadyNotifiedVideoIdsFilePath), true);
        if(in_array($videoId, $alreadyNotifiedVideoIds))
        {
            return $data;
        }
        array_push($alreadyNotifiedVideoIds, $videoId);
        file_put_contents($alreadyNotifiedVideoIdsFilePath, json_encode($alreadyNotifiedVideoIds, JSON_PRETTY_PRINT));

        $publishedAtStr = $snippet['publishedAt'];
        $publishedAt = new DateTimeImmutable($publishedAtStr);
        $algorithmEstablishmentDateTime = new DateTimeImmutable('2024-02-29T00:00:00Z');
        if($publishedAt < $algorithmEstablishmentDateTime)
        {
            return $data;
        }

        // To do: to check livestream starting to be broadcasted, then need to check with YouTube Data API v3? every minute each new channel livestream, what about channels not having streamed since this algorithm establishment?
        $item = getYouTubeOperationalAPI("noKey/videos?part=liveStreamingDetails&id=$videoId");
        // Note that this condition may not be fulfilled.
        if(array_key_exists('liveStreamingDetails', $item) && !array_key_exists('actualStartTime', $item['liveStreamingDetails']))
        {
            return $data;
        }

        $item = getYouTubeOperationalAPI("videos?part=short&id=$videoId");
        $videoType = $item['short']['available'] ? 'short' : 'video';
        // Could disable notifications if a short has the same title as a previous video for this channel but should define a time threshold otherwise will have a linear growing list and especially if a video with the same title is used later on, I won't be notified like if the same kind of event happen after a while ending up with the same video title.

        /*if($channelHandle === '@LeParisien')
        {
            $lastFrameFilePath = 'last.jpg';
            $command = 'ffmpeg -sseof -3 -i `yt-dlp -g ' . escapeshellarg($videoId) . " | head -n 1` -update 1 -q:v 1 $lastFrameFilePath -y 2>&1";
            // Server returned 403 Forbidden (access denied) maybe due to people using the YouTube operational API instance of this machine, could make this unlisted instance private to solve this issue.
            $result = shell_exec($command);
            die($result);
            $command = "convert ${lastFrameFilePath}[1x1+0+0] txt: 2>&1";
            $result = shell_exec($command);
            die("res: " . $result);
            // (219,56,73)
        }*/

        $message = "[$channelName](https://www.youtube.com/$channelHandle): [$title](https://www.youtube.com/watch?v=$videoId)";
        $message = "New YouTube $videoType: $message!";

        function getStreamContextCreateFormat($array, $arraySeparator, $keyValueSeparator)
        {
            return implode($arraySeparator, array_map(fn($key, $value) => $key . $keyValueSeparator . $value, array_keys($array), array_values($array)));
        }

        $headers = [
            'Content-Type' => 'application/json',
            'Origin' => 'https://www.youtube.com',
            'Authorization' => "SAPISIDHASH $SAPISIDHASH",
            'Cookie' => getStreamContextCreateFormat([
                '__Secure-1PSID' => $__Secure_1PSID,
                '__Secure-1PSIDTS' => $__Secure_1PSIDTS,
                '__Secure-1PAPISID' => $__Secure_1PAPISID,
            ], ';', '=')
        ];

        $opts = [
            'http' => [
                'method' => 'POST',
                'content' => json_encode([
                    'context' => [
                        'client' => [
                            'clientName' => 'WEB',
                            'clientVersion' => '2.20240126.01.00'
                        ]
                    ]
                ]),
                'header' => getStreamContextCreateFormat($headers, "\n", ': ')
            ]
        ];

        $context = stream_context_create($opts);

        $url = 'https://www.youtube.com/youtubei/v1/notification/get_notification_menu';
        // What about possible other notifications?
        // Should save last considered notification with `notificationId` and consider all the new ones and verify the pattern for each new notification.
        // Note that I receive an email when someone answers one of my comment, so except if there are other notifications, marking them all as read seems fine.
        $upload = json_decode(file_get_contents($url, false, $context), true);
        $items = $upload['actions'][0]['openPopupAction']['popup']['multiPageMenuRenderer']['sections'][0]['multiPageMenuNotificationSectionRenderer']['items'];
        $uploadText = $items[0]['notificationRenderer']['shortMessage']['simpleText'];
        if(preg_match("* (uploaded:|a mis en ligne|est en direct:) *", $uploadText) !== 1)
        {
            $message .= ' Unable to read notifications, investigate renewing credentials!';
        }

        // Could download videos temporarily, so that if they are removed and reupload, I understand why they were removed.
        sendMatrixMessage($message);

        return $data;
    }

?>
